
import java.text.SimpleDateFormat
// import java.util.regex.Pattern
import java.util.regex.Matcher
import java.time.*
import java.io.File.*
// import java.nio.file.Paths;

def Date = new SimpleDateFormat("yyyyMMdd").format(new Date())
def Start_time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())
def FAILED_STAGE 

def check_Change_Number(parameter_type,value)
{

	if (parameter_type == "Change_Number" && (value =~ /^CR\d{5}$/ || value =~ /^DF\d{5}$/))
	{
		return true
	}else if (parameter_type == "Commit_ID"  && (value =~ /^HEAD$/ || value =~ /^[a-z0-9]{8,40}$/))
	{
		return true
	}
	
	// default return
	return false
	
}

def split_typeOf_file_change(GIT_LIST_CHANGE)
{
	AllChangeFilesArray   = []
	HDFS_FilesArray       = []
	DB_Impala_FilesArray  = []
	DB_Vertica_FilesArray = []
	DB_MySQL_FilesArray   = []
	S_ETL_FilesArray      = []
	S_SIS_FilesArray      = []

	// def ElementData = []
	GIT_LIST_CHANGE.split('\n').each {
		// FilesArray << it
		
		(action, filename) = it.tokenize( '\t' )

		// Array 2 Dimaintion
		// action = getActionRollback(action)
		AllChangeFilesArray << [action,filename]

		// Add file split by server deploy
		if (filename.startsWith("HDFS"))
		{
			HDFS_FilesArray << [action,filename]
		}
		else if (filename.startsWith("DB_Impala") & filename.toLowerCase().endsWith(".sql"))
		{
			DB_Impala_FilesArray << [action,filename]
		}
		else if (filename.startsWith("DB_Vertica") & filename.toLowerCase().endsWith(".sql"))
		{
			DB_Vertica_FilesArray << [action,filename]
		}
		else if (filename.startsWith("DB_MySQL") & filename.toLowerCase().endsWith(".sql"))
		{
			DB_MySQL_FilesArray << [action,filename]
		}
		else if (filename.startsWith("Server_ETL"))
		{
			S_ETL_FilesArray << [action,filename]
		}
		else if (filename.startsWith("Server_SIS"))
		{
			S_SIS_FilesArray << [action,filename]
		}							
	}
}

def End_time(){
    def endTime = currentBuild.startTimeInMillis + currentBuild.duration
    def endTimeString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(endTime))
    return endTimeString   
}

pipeline {
	
	agent none
            
    environment {
       //Working = "${Working}"
       //Branch  = "develop"
       
       //DF_Current_Version  = "HEAD"
       //DF_Previous_Version = "HEAD~1"
       
       Node_Master = "AEP_HMN"
	   Node_HDFS   = "HDFS"
	   Node_SIS    = "AEP_SIS"
	   
	   Branch_Main        = "main"
	   Branch_Destination = "develop"
	   Branch_Source      = "features"
	   Pipeline_type      = "features"
	   
	   RepoURL       = "https://gitlab.com/dpodata/auto_deploy.git"
	//    PullCodePath  = "N/A"
	//    SrcPathName   = "N/A"
	   GIT_CredentialsId = "436e846d-8737-468e-8052-9f7cc077c868"
	   
	   GIT_LIST_CHANGE    = "N/A"
	   GIT_SANITY_RESULT  = "N/A"
	   GIT_LAST_COMMIT_ID = "N/A"
	   REPORT_TEMPLATE    = "N/A"
    }
    
    options {
        timeout(time: 1, unit: 'HOURS')
		skipDefaultCheckout true
    }
    
    parameters {
        string(name: 'Change_Number'     ,defaultValue: "CR1XXX5"                      ,description: 'Format of Change Number\ne.g. CR12345 or DF12345', trim: true)
		string(name: 'Commit_ID'         ,defaultValue: "HEAD"                         ,description: 'SHA on Git Server format 8 or 40 digits\ne.g. HEAD, 358011 or 3580115dde51bd26c94f026aae59bd1e2ac91f02',  trim: true)
		string(name: 'Email_notification',defaultValue: 'suebsakul.a@g-able.com'       ,description: 'Email from which you want to send notification?')
		booleanParam(name: 'Email_Flag'  ,defaultValue: true                           ,description: 'Using email notifications?')
	}
    
    stages {
        stage('Verify Parameter') {

        	steps {
        		echo "============================================================================================="
        		
        		echo "Change Number : [${Change_Number}]"
        		echo "Commit ID : [${Commit_ID}]"
        		
                script {
                    FAILED_STAGE=env.STAGE_NAME
                    try {                    
                      
                      echo "============================================================================================="
                      echo "=         Verify Parameter                                                                  ="
                      echo "============================================================================================="

                      echo "Verify Parameter : Change_Number"
					  def flagVerify = check_Change_Number("Change_Number","${Change_Number}")

					  echo "${flagVerify}"

					  if (flagVerify.toBoolean() == false)
					  {
						echo "Verify Parameter : Change_Number failed value is ${Change_Number}"
						error("Verify Change_Number Error Wrong format [${Change_Number}] should be change following e.g. CR12345 or DF12345")	  
					  }
					  else {
						echo "Verify Parameter : Change_Number passed"
					  }

                    }catch(all){
                      error("Stopping stage ${FAILED_STAGE} is failed")
                    }
                }           		
        	}         	
        }
				
        stage('Pull Code') {
        
		    agent {
		        node
		            { label "${Node_Master}" }
			}

        	steps {
            	echo "============================================================================================="
            
				echo "clear workspace : $PWD"
				deleteDir()

            	echo "${Branch_Destination}"
            	git credentialsId: "${GIT_CredentialsId}", 
            	    url: "${RepoURL}", 
            	    branch: "${Branch_Destination}"
			}
        }        

        stage('List change code') {
		    agent {
		        node
		            { label "${Node_Master}" }       
			}         
        	steps {
        		echo "============================================================================================="

                script {
					
                    FAILED_STAGE=env.STAGE_NAME

                    try {	

						sh("pwd")

						// withCredentials([gitUsernamePassword(credentialsId: "${GIT_CredentialsId}", gitToolName: 'git-tool')])
						withCredentials([gitUsernamePassword(credentialsId: "${GIT_CredentialsId}")])
						{
							// sh("git fetch origin ${Branch_Destination}")
							// sh("git fetch origin ${Branch_Source}/${Change_Number}")
							sh("git fetch origin")

							sh("git checkout ${Branch_Main}")
							sh("git checkout ${Branch_Source}/${Change_Number}")														
														
							sh("git branch")							
							sh("ls -la")							
							GIT_LAST_COMMIT_ID = sh(script:'git log --format="%H" -n 1', returnStdout: true).trim()
							// GIT_LIST_CHANGE = sh(script:'git diff --name-only ${Branch_Destination} ${Branch_Source}/${Change_Number}', returnStdout: true).trim()
							// GIT_LIST_CHANGE = sh(script:'git diff --name-only ${Branch_Main} ${Branch_Source}/${Change_Number}', returnStdout: true).trim()
							GIT_LIST_CHANGE = sh(script:'git diff --name-status ${Branch_Main} ${Branch_Source}/${Change_Number} | python3 CodeDeploy/util/code_diff.py -c CodeDeploy/util/util_config.xlsx', returnStdout: true).trim()

							
							echo "============================================================================================="
							echo "=                                   List of Code Change                                     ="
							echo "============================================================================================="
							echo "${GIT_LIST_CHANGE}"

							writeFile file: Change_Number+"_LIST_FILE_CHANGE.txt", text: GIT_LIST_CHANGE
						}

                    }catch(all){
                      error("Stopping stage ${FAILED_STAGE} is failed")
                    }  						
				}

        	}
        }        

        stage('Build') {
		    agent {
		        node
		            { label "${Node_Master}" }
			}

        	steps {
				echo "============================================================================================="
				
				script {
					
                    FAILED_STAGE=env.STAGE_NAME

					// echo "${GIT_LIST_CHANGE}"

					split_typeOf_file_change(GIT_LIST_CHANGE)

					// def FilesArray=[]
					// GIT_LIST_CHANGE.split().each { 
					// 		FilesArray << it
					// }

					print "Found Change : " + AllChangeFilesArray.size() + " file(s)"
					// print "Found Change" + FilesArray.size() + " file(s)"
					// print FilesArray
					
					for (ElementData in AllChangeFilesArray) {

						(action,filename) = ElementData
						print action + ' = ' + filename
					}

                    try {
						sh("make -f CodeDeploy/util/Makefile")
                    }catch(all){
                      error("Stopping stage ${FAILED_STAGE} is failed")
                    }  						
				}

			}			
		}

        // stage('Deploy HDFS') {
		//     agent {
		//         node
		//             { label "${Node_Master}" }
		// 	}

        // 	steps {
		// 		echo "============================================================================================="

		// 		script {
					
        //             FAILED_STAGE=env.STAGE_NAME

		// 			print "Found " + HDFS_FilesArray.size() + " file(s) change"
					
		// 			if (HDFS_FilesArray.size() == 0)
		// 			{
		// 				//notificate but build result's success
		// 				catchError(stageResult: 'UNSTABLE', buildResult: currentBuild.result) {
		// 					echo("Skip because not file deploy.")
		// 					sh "exit 1"
		// 				}
		// 			}
		// 			else
		// 			{
		// 				try {

		// 					for (ElementData in HDFS_FilesArray) {

		// 						(action,filename) = ElementData
		// 						print action + ' = ' + filename
		// 					}						

		// 				}catch(all){
		// 					error("Stopping stage ${FAILED_STAGE} is failed")
		// 				}
		// 			}  						
		// 		}

		// 	}			
		// }		

        // stage('Deploy DB Impala') {
		//     agent {
		//         node
		//             { label "${Node_Master}" }
		// 	}

        // 	steps {
		// 		echo "============================================================================================="

		// 		script {
					
        //             FAILED_STAGE=env.STAGE_NAME

		// 			print "Found " + DB_Impala_FilesArray.size() + " file(s) change"

					
		// 			if (DB_Impala_FilesArray.size() == 0)
		// 			{
		// 				catchError(stageResult: 'UNSTABLE', buildResult: currentBuild.result) {
		// 					echo("Skip because not file deploy.")
		// 					sh "exit 1"
		// 					// unstable('Skip because not file deploy.')
		// 				}
		// 			}
		// 			else
		// 			{
		// 				try {

		// 					for (ElementData in DB_Impala_FilesArray) {

		// 						(action,filename) = ElementData
		// 						print action + ' = ' + filename
		// 					}							

		// 				}catch(all){
		// 					error("Stopping stage ${FAILED_STAGE} is failed")
		// 				}
		// 			}  						
		// 		}

		// 	}			
		// }		

        // stage('Deploy DB Vertica') {
		//     agent {
		//         node
		//             { label "${Node_Master}" }
		// 	}

        // 	steps {
		// 		echo "============================================================================================="

		// 		script {
					
        //             FAILED_STAGE=env.STAGE_NAME

		// 			print "Found " + DB_Vertica_FilesArray.size() + " file(s) change"

					
		// 			if (DB_Vertica_FilesArray.size() == 0)
		// 			{
		// 				catchError(stageResult: 'UNSTABLE', buildResult: currentBuild.result) {
		// 					echo("Skip because not file deploy.")
		// 					sh "exit 1"
		// 					// unstable('Skip because not file deploy.')
		// 				}
		// 			}
		// 			else
		// 			{
		// 				try {

		// 					for (ElementData in DB_Vertica_FilesArray) {

		// 						(action,filename) = ElementData
		// 						print action + ' = ' + filename
								
		// 						if (filename.contains("/ddl/") & filename.contains("_ddl.sql"))
		// 						{
		// 							sh "python3 CodeDeploy/util/db_deploy.py -e ${Pipeline_type} -p DB_Vertica -t ddl  -f ${filename} -a ${action} -b ${BUILD_NUMBER}"
		// 						}
		// 						else if  (filename.contains("/data/") & filename.contains("_data.sql"))
		// 						{
		// 							sh "python3 CodeDeploy/util/db_deploy.py -e ${Pipeline_type} -p DB_Vertica -t data -f ${filename} -a ${action} -b ${BUILD_NUMBER}"
		// 						}
		// 						else {
		// 							echo("Skip because unknow action file deploy.")
		// 						}

		// 					}							

		// 				}catch(Exception e){
		// 					echo 'Exception occurred: ' + e.toString()
		// 					error("Stopping stage ${FAILED_STAGE} is failed")
		// 				}
		// 			}  										
		// 		}

		// 	}			
		// }		

        stage('Deploy DB MySQL') {
		    agent {
		        node
		            { label "${Node_Master}" }
			}

        	steps {
				echo "============================================================================================="

				script {
					
                    FAILED_STAGE=env.STAGE_NAME

					print "Found " + DB_MySQL_FilesArray.size() + " file(s) change"

					
					if (DB_MySQL_FilesArray.size() == 0)
					{
						catchError(stageResult: 'UNSTABLE', buildResult: currentBuild.result) {
							echo("Skip because not file deploy.")
							sh "exit 1"
							// unstable('Skip because not file deploy.')
						}
					}
					else
					{
						try {

							for (ElementData in DB_MySQL_FilesArray) {

								(action,filename) = ElementData
								print action + ' = ' + filename

								if (filename.contains("/ddl/") & filename.contains("_ddl.sql"))
								{
									sh "python3 CodeDeploy/util/db_deploy.py -e ${Pipeline_type} -p DB_MySQL -t ddl  -f ${filename} -a ${action} -b ${BUILD_NUMBER}"
								}
								else if  (filename.contains("/data/") & filename.contains("_data.sql"))
								{
									sh "python3 CodeDeploy/util/db_deploy.py -e ${Pipeline_type} -p DB_MySQL -t data -f ${filename} -a ${action} -b ${BUILD_NUMBER}"
								}
								else {
									echo("Skip because unknow action file deploy.")
								}
							}							

						}catch(all){
							error("Stopping stage ${FAILED_STAGE} is failed")
						}
					}  					
				}

			}			
		}		

        stage('Deploy ETL') {
		    agent {
		        node
		            { label "${Node_Master}" }
			}

        	steps {
				echo "============================================================================================="

				script {
					
                    FAILED_STAGE=env.STAGE_NAME

					print "Found " + S_ETL_FilesArray.size() + " file(s) change"
					
					if (S_ETL_FilesArray.size() == 0)
					{
						catchError(stageResult: 'UNSTABLE', buildResult: currentBuild.result) {
							echo("Skip because not file deploy.")
							sh "exit 1"
							// unstable('Skip because not file deploy.')
						}
					}
					else
					{
						try {

							//Backup previous code
							sh("cp -a /gfs_landing1/dtac/OLAP /gfs_landing1/dtac/OLAP_bak_${BUILD_NUMBER}")
							sh("cp -a /gfs_landing1/dtac/loaderUtil /gfs_landing1/dtac/loaderUtil_bak_${BUILD_NUMBER}")
							sh("cp -a /gfs_landing1/dtac/Outbound_Process /gfs_landing1/dtac/Outbound_Process_bak_${BUILD_NUMBER}")
							sh("cp -a /gfs_landing1/USER_DATA /gfs_landing1/USER_DATA_bak_${BUILD_NUMBER}")

							for (ElementData in S_ETL_FilesArray) {

								(action,filename) = ElementData
								print action + ' = ' + filename

								file = new File(filename)
								// print "getName: "+ file.getName()
								// print "getParent: "+ file.getParent()

								getParent = file.getParent()
								// print files[0].path
								full_filename = filename.replace("Server_ETL", "")
								pathParent    = getParent.replace("Server_ETL", "").replace("\\", "/")

								// print full_filename						
								// print "[" + pathParent + "]"

								// Check path is not exists
								if(!fileExists(pathParent))
								{
									sh("mkdir -p ${pathParent}")
								}

								if(!fileExists(full_filename))
								{
									echo "Found file : ${full_filename}"
								}

								sh("cp -a ${filename} ${full_filename}")

							}							

							// Full Deploy
							// Deploy replace all file on repository to server
							// Deploy path /gfs/landing/dtac/OLAP							
							// sh("cp -r Server_ETL/gfs/landing/dtac/OLAP /gfs/landing/dtac/OLAP")


						}catch(all){

							// sh("mv /gfs_landing1/dtac/OLAP_bak_${BUILD_NUMBER}/log /gfs_landing1/dtac/OLAP/")
							// sh("mv /gfs_landing1/dtac/Outbound_Process_bak_${BUILD_NUMBER}/EDW /gfs_landing1/dtac/Outbound_Process/")

							// sh("mv /gfs_landing1/dtac/OLAP_bak_${BUILD_NUMBER} /gfs_landing1/dtac/OLAP")
							// sh("mv /gfs_landing1/dtac/loaderUtil_bak_${BUILD_NUMBER} /gfs_landing1/dtac/loaderUtil")
							// sh("mv /gfs_landing1/dtac/Outbound_Process_bak_${BUILD_NUMBER} /gfs_landing1/dtac/Outbound_Process")
							// sh("mv /gfs_landing1/USER_DATA_bak_${BUILD_NUMBER} /gfs_landing1/USER_DATA")

							error("Stopping stage ${FAILED_STAGE} is failed")
						}
					}  							
				}

			}			
		}		

        stage('Deploy SIS') {
		    agent {
		        node
		            { label "${Node_SIS}" }
			}

        	steps {
				echo "============================================================================================="

				echo "clear workspace : $PWD"
				deleteDir()

            	echo "${Branch_Destination}"
            	git credentialsId: "${GIT_CredentialsId}", 
            	    url: "${RepoURL}", 
            	    branch: "${Branch_Source}/${Change_Number}"


				script {
					
                    FAILED_STAGE=env.STAGE_NAME

					print "Found " + S_SIS_FilesArray.size() + " file(s) change"

					
					if (S_SIS_FilesArray.size() == 0)
					{
						catchError(stageResult: 'UNSTABLE', buildResult: currentBuild.result) {
							echo("Skip because not file deploy.")
							sh "exit 1"
							// unstable('Skip because not file deploy.')
						}
					}
					else
					{
						try {

							//Backup previous code

							// reduce on sis only 1 path deploy
							// sh("mv /gfs/landing/dtac/aepsftp /gfs/landing/dtac/aepsftp_bak_${BUILD_NUMBER}")							
							sh("cp -a /gfs_landing1/dtac/aepsftp /gfs_landing1/dtac/aepsftp_bak_${BUILD_NUMBER}")

							// Partial Deploy
							def file = ""
							for (ElementData in S_SIS_FilesArray) {

								(action,filename) = ElementData
								print action + ' = ' + filename

								file = new File(filename)								
								// print "getName: "+ file.getName()
								// print "getParent: "+ file.getParent()

								getParent = file.getParent()
								// print files[0].path
								full_filename = filename.replace("Server_SIS", "")
								pathParent    = getParent.replace("Server_SIS", "").replace("\\", "/")

								// print full_filename						
								// print "[" + pathParent + "]"

								// Check path is not exists
								if(!fileExists(pathParent))
								{
									sh("mkdir -p ${pathParent}")
								}

								if(!fileExists(full_filename))
								{
									echo "Found file : ${full_filename}"
								}

								sh("cp -a ${filename} ${full_filename}")
							}

							// Move log to current path						
							// sh("mv /gfs_landing1/dtac/aepsftp_bak_${BUILD_NUMBER}/feedcomplete /gfs_landing1/dtac/aepsftp/")

							// Full Deploy
							// Deploy replace all file on repository to server
							// Deploy path /gfs/landing/dtac/aepsftp							
							// sh("cp -r Server_SIS/gfs/landing/dtac/aepsftp /gfs/landing/dtac/aepsftp")

							// Deploy path /gfs_landing1/dtac							
							// sh("cp -r Server_SIS/gfs_landing1/dtac /gfs_landing1/dtac")


						}catch(all) {					
							error("Stopping stage ${FAILED_STAGE} is failed")
						}
					}  								
				}

			}			
		}		

        stage('Sanity Test') {
		    agent {
		        node
		            { label "${Node_Master}" }
			}         
        	steps {
        		echo "============================================================================================="

				// error("Skip because under construction...")

				script {

					// currentBuild.result = 'FAILURE'	
					

                    FAILED_STAGE=env.STAGE_NAME
					
                    try {				
						currentBuild.result = "SUCCESS"
						// currentBuild.currentResult = 'SUCCESS'


						//write deployment from
						writeFile file: Change_Number+"_LIST_FILE_CHANGE.txt", text: GIT_LIST_CHANGE
						writeFile file: "deploy_result.txt", text: "DB_Impala|"+DB_Impala_FilesArray.size()+"|0\nDB_MySQL|"+DB_MySQL_FilesArray.size()+"|0\nDB_Vertica|"+DB_Vertica_FilesArray.size()+"|0\nHDFS|"+HDFS_FilesArray.size()+"|0\nServer_ETL|"+S_ETL_FilesArray.size()+"|0\nServer_SIS|"+S_SIS_FilesArray.size()+"|0\n"
						sh("echo '' >> ${Change_Number}_LIST_FILE_CHANGE.txt")
						sh("cat ${Change_Number}_LIST_FILE_CHANGE.txt | CodeDeploy/util/gen_rpt.sh deploy_result.txt report_${Change_Number}.html ${Change_Number} ${BUILD_NUMBER}")


						REPORT_TEMPLATE = sh(script:'cat report_${Change_Number}.html', returnStdout: true).trim()


                    } catch(all){
                      error("Stopping stage ${FAILED_STAGE} is failed")
                    }
				}
        	}
        }
                
	}

	post {		

		always {
            script {
                if (currentBuild.result == 'UNSTABLE' || currentBuild.result == 'FAILURE'){
                    currentBuild.result = 'SUCCESS'
                }

				
				print currentBuild.result
            }
        }

		failure {
			
			echo "failure"

			node('master')
			{
				script {
					if ("${params.Email_Flag}" == 'true'){

						// GIT_LIST_CHANGE.split().each { 							
						// 	if (it.startsWith("Server_SIS"))
						// 	{
						// 		FilesDeployArray << it
						// 	}													
						// }

						
						writeFile file: Change_Number+"_LIST_FILE_CHANGE.txt", text: GIT_LIST_CHANGE
						
						writeFile file: "report_"+Change_Number+".html", text: REPORT_TEMPLATE
						

						emailext (
								to: "${params.Email_notification}",
								from: "kul1977@gmail.com",
								subject: "Pipeline : ${Change_Number} ${JOB_NAME} - ${Date} - Build ${BUILD_NUMBER} - ${currentBuild.result}!",
								mimeType: 'text/html',
								attachLog: true, attachmentsPattern: Change_Number+"_LIST_FILE_CHANGE.txt",
								body:"""Dear Team,<br><br>
								Pipeline : ${JOB_NAME} <br>
								Build : ${BUILD_NUMBER}<br>
								Start : ${Start_time} <br>
								End   : """+End_time()+""" <br>
								Duration : ${currentBuild.durationString.minus(' and counting')}<br>
								Status : <span style="background-color: #e71b00"><b>${currentBuild.result}</b></span><br>
								Description : <br>
											LAST COMMIT ID ${GIT_LAST_COMMIT_ID}<br>
								
								<br>
								Check console output at $BUILD_URL to view the results.<br><br>
								Investigate more from web jenkins of team <br>
								Regards,<br>
								Automate Deploy CIFlow(Jenkins)"""
							
							)
					} else {
						echo "Skip stage --> Declarative: Post Actions because not using email notifications"
					}
				}
			}

		}

		success {
			echo "success"

			node('master')
			{
				script {
					if ("${params.Email_Flag}" == 'true'){

						def Timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date())
						
						writeFile file: Change_Number+"_LIST_FILE_CHANGE.txt", text: GIT_LIST_CHANGE					
						writeFile file: "report_"+Change_Number+"_"+BUILD_NUMBER+"_"+Timestamp+".html", text: REPORT_TEMPLATE

						emailext (
								to: "${params.Email_notification}",
								from: "kul1977@gmail.com",
								subject: "Pipeline : ${JOB_NAME} - ${Date} - Build ${BUILD_NUMBER} - ${currentBuild.result}!",
								attachLog: true, 
								attachmentsPattern: "report_"+Change_Number+"_"+BUILD_NUMBER+"_"+Timestamp+".html",								
								mimeType: 'text/html',
								body:"""Dear Team,<br><br>
								Pipeline : ${JOB_NAME} <br>
								Build : ${BUILD_NUMBER}<br>
								Start : ${Start_time} <br>
								End   : """+End_time()+""" <br>
								DURATION : ${currentBuild.duration} sec(s)<br>
								Status : <span style="background-color: #98FB98"><b>${currentBuild.result}</b></span><br>
								Description : Create Branch : ${Branch_Destination}/${Change_Number}<br>											  
								Sanity Result : ${GIT_SANITY_RESULT} on COMMIT_ID ${Commit_ID}<br>
								Check console output at $BUILD_URL to view the results.<br><br>
								Investigate more from web jenkins of team <br>
								Regards,<br>
								Automate Deploy CIFlow(Jenkins)"""
							
							)
					} else {
						echo "Skip stage --> Declarative: Post Actions because not using email notifications"
					}
				}
			}			
		}

	}
}
