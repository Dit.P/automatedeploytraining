
import java.text.SimpleDateFormat
// import java.util.regex.Pattern
import java.util.regex.Matcher
import java.time.*

def Date = new SimpleDateFormat("yyyyMMdd").format(new Date())
def Start_time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())
def FAILED_STAGE 

def check_Change_Number(parameter_type,value)
{

	if (parameter_type == "Change_Number" && (value =~ /^CR\d{5}$/ || value =~ /^DF\d{5}$/))
	{
		return true
	}else if (parameter_type == "Commit_ID"  && (value =~ /^HEAD$/ || value =~ /^[a-z0-9]{8,40}$/))
	{
		return true
	}
	
	// default return
	return false
	
}

def End_time(){
    def endTime = currentBuild.startTimeInMillis + currentBuild.duration
    def endTimeString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(endTime))
    return endTimeString   
}

pipeline {
	
	agent none
            
    environment {
       //Working = "${Working}"
       //Branch  = "develop"
       
       //DF_Current_Version  = "HEAD"
       //DF_Previous_Version = "HEAD~1"
       
       Node_Master = "AEP_HMN"
	   
	   Branch_Main        = "main"
	   Branch_Source      = "develop"
	   Branch_Destination = "features"
	   Pipeline_type = "features"
	   
	   RepoURL       = "https://gitlab.com/dpodata/auto_deploy.git"
	//    PullCodePath  = "N/A"
	//    SrcPathName   = "N/A"
	   GIT_CredentialsId = "436e846d-8737-468e-8052-9f7cc077c868"
	   
	   GIT_RESULT_PUSH   = "N/A"
	   GIT_SANITY_RESULT = "N/A"

    }
    
    options {
        timeout(time: 1, unit: 'HOURS')
    }
    
    parameters {
        string(name: 'Change_Number'     ,defaultValue: "CR20004"                      ,description: 'Format of Change Number\ne.g. CR12345 or DF12345', trim: true)
        string(name: 'Commit_ID'         ,defaultValue: "HEAD"                         ,description: 'SHA on Git Server format 8 or 40 digits\ne.g. HEAD, 358011 or 3580115dde51bd26c94f026aae59bd1e2ac91f02',  trim: true)
		text(name: 'Description'         ,defaultValue: "This Project create for nono" ,description: 'Descript keep on Git Server')
		string(name: 'Email_notification',defaultValue: 'suebsakul.a@g-able.com'       ,description: 'Email from which you want to send notification?')
		booleanParam(name: 'Email_Flag'  ,defaultValue: true                           ,description: 'Using email notifications?')
	}
    
    stages {
        stage('Verify Parameter') {

        	steps {
        		echo "============================================================================================="
        		
        		echo "Change Number : [${Change_Number}]"
        		echo "Commit ID : [${Commit_ID}]"
        		
                script {
                    FAILED_STAGE=env.STAGE_NAME
                    try {                    
                      
                      echo "============================================================================================="
                      echo "=         Verify Parameter                                                                  ="
                      echo "============================================================================================="

                      echo "Verify Parameter : Change_Number"
					  def flagVerify = check_Change_Number("Change_Number","${Change_Number}")

					  echo "${flagVerify}"

					  if (flagVerify.toBoolean() == false)
					  {
						echo "Verify Parameter : Change_Number failed value is ${Change_Number}"
						error("Verify Change_Number Error Wrong format [${Change_Number}] should be change following e.g. CR12345 or DF12345")	  
					  }
					  else {
						echo "Verify Parameter : Change_Number passed"
					  }
					  
                      echo "Verify Parameter : Commit_ID"
					  flagVerify = check_Change_Number("Commit_ID","${Commit_ID}")

					  echo "${flagVerify}"

					  if (flagVerify.toBoolean() == false)
					  {
						echo "Verify Parameter : Commit_ID failed value is ${Commit_ID}"
						error("Verify Commit_ID Error Wrong format [${Commit_ID}] should be change following e.g. HEAD, 358011 or 3580115dde51bd26c94f026aae59bd1e2ac91f02")	  
					  }
					  else {
						echo "Verify Parameter : Commit_ID passed"
					  }					  
                      
                    }catch(all){
                      error("Stopping stage ${FAILED_STAGE} is failed")
                    }                   
                }           		
        	}         	
        }
				
        stage('Pull Code') {
        
		    agent {
		        node
		            { label "${Node_Master}" }
			}

        	steps {
            	echo "============================================================================================="
            
				echo "clear workspace : $PWD"
				deleteDir()

            	echo "${Branch_Main}"
            	git credentialsId: "${GIT_CredentialsId}", 
            	    url: "${RepoURL}", 
            	    branch: "${Branch_Main}"
			}
        }        

        stage('Create new Branch on Features') {
		    agent {
		        node
		            { label "${Node_Master}" }       
			}         
        	steps {
        		echo "============================================================================================="

                script {
                    FAILED_STAGE=env.STAGE_NAME
                    try {
						
						
						sh("pwd")
						sh("ls -ltr")
						sh("git branch -r --contains ${Commit_ID} | grep ${Branch_Source}")
						sh("git switch -c ${Branch_Destination}/${Change_Number} ${Commit_ID}")
						sh("git branch")
						
						withCredentials([gitUsernamePassword(credentialsId: "${GIT_CredentialsId}", gitToolName: 'git-tool')]) {
							// sh("git tag -a '112233' -m '${Description}'")
							GIT_RESULT_PUSH = sh(script:'git push --set-upstream origin ${Branch_Destination}/${Change_Number}', returnStdout: true).trim()
						}

                    }catch(all){
                      error("Stopping stage ${FAILED_STAGE} is failed")
                    }  						
				}

        	}
        }        
        
        stage('Sanity Test') {
		    agent {
		        node
		            { label "${Node_Master}" }       
			}         
        	steps {
        		echo "============================================================================================="
				
				script {
					GIT_SANITY_RESULT = sh(script:'git branch -r --contains ${Commit_ID} | grep origin/${Branch_Destination}/${Change_Number}', returnStdout: true).trim()
				}

				echo "clear workspace : $PWD"
				deleteDir()
        	}
        }        
                
	}

	post {
		failure {
			echo "failure"

			script {
                if ("${params.Email_Flag}" == 'true'){
                    emailext (
							to: "${params.Email_notification}",
							from: "kul1977@gmail.com",
							subject: "Process : ${JOB_NAME} - ${Date} - Build ${BUILD_NUMBER} - ${currentBuild.result}!",
							mimeType: 'text/html',
							body:"""Dear Team,<br><br>
							Pipeline process : ${JOB_NAME} <br>
							Build : ${BUILD_NUMBER}<br>
							Start : ${Start_time} <br>
							End   : """+End_time()+""" <br>
							Duration : ${currentBuild.durationString.minus(' and counting')}<br>
							Status : <span style="background-color: #e71b00"><b>${currentBuild.result}</b></span><br>
							Description : ${GIT_RESULT_PUSH}<br>
							              Create Branch : ${Branch_Destination}/${Change_Number}<br>
							              ${Description}<br>
							Sanity Result : ${GIT_SANITY_RESULT} on COMMIT_ID ${Commit_ID}<br>
							Check console output at $BUILD_URL to view the results.<br><br>
							Investigate more from web jenkins of team <br>
							Regards,<br>
							Automate Deploy CIFlow(Jenkins)"""
                        
                        )
                } else {
                    echo "Skip stage --> Declarative: Post Actions because not using email notifications"
                }
			}

		}

		success {
			echo "success"

            script {
                if ("${params.Email_Flag}" == 'true'){
                    emailext (
							to: "${params.Email_notification}",
							from: "kul1977@gmail.com",
							subject: "Process : ${JOB_NAME} - ${Date} - Build ${BUILD_NUMBER} - ${currentBuild.result}!",
							mimeType: 'text/html',
							body:"""Dear Team,<br><br>
							Pipeline process : ${JOB_NAME} <br>
							Build : ${BUILD_NUMBER}<br>
							Start : ${Start_time} <br>
							End   : """+End_time()+""" <br>
							Duration : ${currentBuild.durationString.minus(' and counting')}<br>
							Status : <span style="background-color: #98FB98"><b>${currentBuild.result}</b></span><br>
							Description : ${GIT_RESULT_PUSH}<br>
							              Create Branch : ${Branch_Destination}/${Change_Number}<br>
							              ${Description}<br>
							Sanity Result : ${GIT_SANITY_RESULT} on COMMIT_ID ${Commit_ID}<br>
							Check console output at $BUILD_URL to view the results.<br><br>
							Investigate more from web jenkins of team <br>
							Regards,<br>
							Automate Deploy CIFlow(Jenkins)"""
                        
                        )
                } else {
                    echo "Skip stage --> Declarative: Post Actions because not using email notifications"
                }
            }			
		}

	}
}
