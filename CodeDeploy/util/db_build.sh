#!/usr/bin/bash
##################################################################################################
# SCRIPT NAME      : db_build.sh
# DESCRIPTION      : propose for build for change schema variable list files
#                      - {tablename}_data.sql
#                      - {tablename}_ddl.sql
#                    on Database build stage
# AUTHOR           : Suebsakul Ampa
# CREATED DATE     : 03/11/2021
# USAGE            : ./db_build.sh {ENV} {DB_PATH} {SQL_TYPE}
#-------------------------------------------------------------------------------------------------
# CHANGED REVISION
#
# UPDATED BY       : Suebsakul Ampa
# UPDATED DATE     : 03/11/2021
# Initial version
#
# UPDATED BY       : 
# UPDATED DATE     : 
#
##################################################################################################

## ======= Define Variables ======= ##
ENV=$1
DB_PATH=$2
SQL_TYPE=$3

echo "ENV:       $ENV"
echo "Root Path: $DB_PATH"
echo "Sql Type:  $SQL_TYPE"

getSchema()
{
    ENV=$1
    DB_PATH=$2
    SCHEMA=$3

    # for debug
    # echo "------------------------------------------"
    # echo "- ENV : ${ENV}                           -"
    # echo "- DB_PATH : ${DB_PATH}                   -"
    # echo "- SCHEMA : ${SCHEMA}                     -"
    # echo "------------------------------------------"

    declare -A arrSchema_MySQL
    arrSchema_MySQL[DB_AEP]="AEP_1|AEP_2|AEP_3"

    if [ $ENV == "Develop" ];then
        getSchema=$(echo ${arrSchema_MySQL[DB_AEP]} | cut -d '|' -f 1)
    elif [ $ENV == "QA" ];then
        getSchema=$(echo ${arrSchema_MySQL[DB_AEP]} | cut -d '|' -f 2)
    elif [ $ENV == "Production" ];then
        getSchema=$(echo ${arrSchema_MySQL[DB_AEP]} | cut -d '|' -f 3)
    else
        echo "ERROR : DPDB002 Unknow Environment [$ENV] on step build.."
        return 99        
    fi

    echo "$getSchema"
}

# Check found filename not standard
echo "------------------------------------------"
echo "-         Show wrong filename            -"
echo "------------------------------------------"
find $DB_PATH/ -type f ! -iname *_data.sql -a ! -iname *_ddl.sql | grep .sql$
WRONG_FILENAME=$(find $DB_PATH/ -type f ! -iname *_data.sql -a ! -iname *_ddl.sql | grep .sql$ | wc -l)

if [ $WRONG_FILENAME -ne 0 ];then
    echo "ERROR : DPDB003 Found filename wrong format on step build.."
    exit 99
else
    echo "PASSED : Wrong filename $WRONG_FILENAME file"
fi

# Change schema variable to real value
find $DB_PATH/ -type f -iname *_$SQL_TYPE.sql | while read filename
do
    echo "Found file : $filename"
    
    # Schema
    SCHEMA=$(echo $filename | cut -d '/' -f 2)

    ValueSchema=$(getSchema $ENV $DB_PATH $SCHEMA)
    echo "ValueSchema [$ValueSchema]"

    # replace schema variable

    if [ $DB_PATH == "DB_MySQL" ];then
        sed -e 's/${DB_AEP}\./'"${ValueSchema}"'./g' \
            -e 's/${DB_AEPA}\./aep_1234./g' $filename > tmpfile
    elif [ $DB_PATH == "DB_Vertica" ];then
        sed -e 's/${DB_AEP}\./aep_1234./g' $filename > tmpfile
    else
        echo "ERROR : DPDB002 Unknow type of Database [$DB_PATH] on step build.."
        exit 99        
    fi

    diff $filename tmpfile    
    iRet=$?

    if [ $iRet -eq 1 ];then
        
        echo "------------------------------------------"
        echo "-      PASSED Found file have change     -"
        echo "------------------------------------------"
        sleep 2

        mv tmpfile $filename
    else
        echo "ERROR : DPDB001 file.sql: [$filename] not change on step build.."
        exit 99
    fi

done
