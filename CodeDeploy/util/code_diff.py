#!/usr/local/bin/python3
##################################################################################################
# SCRIPT NAME      : code_diff.py
# DESCRIPTION      : propose compare code between commit id
# AUTHOR           : Suebsakul Ampa
# CREATED DATE     : 03/11/2021
# USAGE            : git diff --name-status main features/CR20001 | python3 CodeDeploy/util/code_diff.py -c CodeDeploy/util/util_config.xlsx
#-------------------------------------------------------------------------------------------------
# CHANGED REVISION
#
# UPDATED BY       : Suebsakul Ampa
# UPDATED DATE     : 03/11/2021
# Initial version
#
# UPDATED BY       : Suebsakul Ampa
# UPDATED DATE     : 
#
##################################################################################################

import pandas as pd
import logging
import sys, getopt
import os, shutil
import re
from datetime import datetime
import time
# import glob
import argparse
import warnings

# Close
# UserWarning: Data Validation extension is not supported and will be removed
warnings.filterwarnings('ignore', category=UserWarning, module='openpyxl')

from pathlib import Path
#module sql statment
# import sqlparse
# import sql_metadata

#module DB
# import mariadb
# import vertica_python
# from impala.dbapi import connect

import uuid
# import difflib
# from difflib import Differ


#change directory
# only debug mode
# os.chdir('D:\\Workspace\\TOP_Crude_Flex\\Monitor\\etl\\bin\\')
PWD = os.getcwd()

CONF_RULES_FILTER = '/CodeDeploy/util/util_config.xlsx'

os.chdir('D:\\Workspace\\Auto_Deploy\\CodeDeploy\\' if "win" in sys.platform else PWD+'/CodeDeploy/')

# insert at 1, 0 is the script path (or '' in REPL)
# sys.path.insert(0, '../libs')
import python_libs   as Mylib
# import folder_libs   as Folderlib
# import variable_libs as Varlib

# sys.path.insert(0, '..')
# from libs import python_libs as Mylib
# from libs import folder_libs as Folderlib

#----------------------------------------- Variable ------------------------------------------
# define veriable
Mylib.EXEC_PARA='-c <Full config deploy action.xlsx>'
DB_CHAR  = "utf8"
                   
# folder_lib = Folderlib._folder()


if len(sys.argv) <= 1 :
    Mylib._usage(sys.argv)

commandline_argment = Mylib._commandline_argment()

try:
    opts, args = getopt.getopt(sys.argv[1:],"hc:",["help","config="])

    for (o, v) in opts:
        if o == '-c':
            # file_load = v
            commandline_argment.add('config',v)
        elif o == '-h':
            Mylib._usage(sys.argv)
        else:
            # sys.stderr.write("%s: illegal option '%s'\n" % (args[0], o))
            # log.error("%s: illegal option '%s'\n" % (args[0], o))
            Mylib._usage(sys.argv)
except:
    Mylib._usage(sys.argv)

# #assign default argment
# if commandline_argment.get('Mode') == None:
#     commandline_argment.add('Mode','default')

#assign uuid.uuid1()
UUID = str(uuid.uuid1())
commandline_argment.add('uuid',UUID)

#----------------------------------------- Function ------------------------------------------
def load_rules_replaces(file_rules_replace):

    df = pd.read_excel(file_rules_replace, sheet_name='deploy_action', engine = "openpyxl")

    # Filter only active and same DB Path
    df_rules_replace = df[(df.Status.str.lower() == 'Active'.lower())]
    # log.info('Filename : %s on Sheet: = %d record(s)' % (file_rules_replace, len(df_rules_replace)))                                

    return df_rules_replace


#------------------------------------------- Main --------------------------------------------

# define loggin
start_time = time.time()
log = logging.getLogger() # 'root' Logger

# list argments
# commandline_argment.list()

# print("Argv params:\n ", sys.argv)

list_file_change = []
if not sys.stdin.isatty():
    # print("Command Line args: \n", sys.stdin.readlines())
    list_file_change = sys.stdin.readlines()

try:

    # load rule replace
    # log.info("load deploy action")
    df_deploy_action = load_rules_replaces(PWD + CONF_RULES_FILTER)   

    df_ROOT_PATH = df_deploy_action.drop_duplicates(subset = ["ROOT_PATH"])
 

    # for index, row in df_ROOT_PATH.iterrows():
        # log.info('Deploy Action[%2d] : %s' % (index+1, row['ROOT_PATH']))

    
    for filename in list_file_change:
        filename = filename.strip()

        # log.info('--> Filename : %s ' % (filename))

        # # for index, row in df_ROOT_PATH.iterrows():
        for index, row in df_deploy_action.iterrows():

            regular_rule = row['Suffix_file'].replace("*",".*").replace("_","\_")

            if row['ROOT_PATH'] in filename and re.search(regular_rule,filename) and "README.md" not in filename:
            # if row['ROOT_PATH'] in filename:
                # log.info('Filename : %s ' % (filename))
                print(filename)
                break
            
    # add new line on last line
    print()
except Exception as e:
    print(f"Error: {e}")  
    # log.error(f'Error: {e}')     
   
    sys.exit(2)
