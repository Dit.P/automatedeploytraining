import pandas as pd
import openpyxl
import logging
import logging.handlers
import sys, getopt
import os
import re
from datetime import datetime
import time
import glob
import __main__
from beautifultable import BeautifulTable
#from termcolor import colored

import folder_libs   as Folderlib
import variable_libs as Varlib
import inspect

#module DB
import mariadb

EXEC_PARA='N/A'

DB_REPO  = "DPO_REPO"
DB_ROBOT = "DPO_ROBOT"
DB_CHAR  = "utf8"

folder_lib = Folderlib._folder()

def log_namer():
    # This will be called when doing the log rotation
    # default_name is the default filename that would be assigned, e.g. Rotate_Test.txt.YYYY-MM-DD
    # Do any manipulations to that name here, for example this changes the name to Rotate_Test.YYYY-MM-DD.txt
    # base_filename, ext, date = default_name.split(".")
    # (filename,extension) = os.path.splitext(__main__.__file__)
    filename = "test"
    return f'{filename}_'+ datetime.now().strftime('%Y-%m-%d_%H') +'.log'

#----------------------------------------- Logging ------------------------------------------
# logging.basicConfig()
logFormatter = logging.Formatter('[%(asctime)s] {%(filename)s:%(lineno)d} %(levelname)s - %(message)s')
logging.basicConfig(stream=sys.stdout, format='[%(asctime)s] {%(filename)s:%(lineno)d} %(levelname)s - %(message)s')

log = logging.getLogger()
# log.setLevel(logging.DEBUG)
log.setLevel(logging.INFO)

# log_filename = log_namer()
# fileHandler = logging.handlers.TimedRotatingFileHandler(filename="{0}/{1}".format(folder_lib.get("logs"), log_filename), backupCount=5)
# # fileHandler.suffix = "%Y-%m-%d"
# fileHandler.namer = log_namer()
# # fileHandler = logging.FileHandler("{0}/{1}.log".format("..\\logs", "stg_load.log"))
# fileHandler.setFormatter(logFormatter)
# log.addHandler(fileHandler)

#----------------------------------------- Function ------------------------------------------
def _usage(argv) :
    print ('Invalid parameters:',argv[0],EXEC_PARA)
    sys.exit(2)

class _commandline_argment:
    def __init__(self):
        self._arg = {}

    def add(self, argment_name, value):
        # self._tables.append(table_name);
        if argment_name.upper() == "DB_PATH"  or \
           argment_name.upper() == "FILE_SQL" or \
           argment_name.upper() == "SQL_TYPE":
            
            self._arg[argment_name.upper()]=value
        else:
            self._arg[argment_name.upper()]=value.upper()

    def update(self, argment_name, value):
        # self._tables.append(table_name);
        if argment_name.upper() == "DB_PATH"  or \
           argment_name.upper() == "FILE_SQL" or \
           argment_name.upper() == "SQL_TYPE":
            self._arg[argment_name.upper()]=value
        else:
            self._arg[argment_name.upper()]=value.upper()        

    def get(self, argment_name):
        # self._tables.append(table_name);
        if argment_name.upper() in self._arg.keys():        
            return self._arg[argment_name.upper()]
        else:
            return None

    def check(self, argment_name, chk_value):
        if self.get(argment_name) == chk_value.upper():
            return True
        else:
            return False

    def list(self):
        count=0
        for arg in self._arg:
            count+=1;
            log.info(" %d : argment : %s = '%s'" % (count,arg,str(self._arg[arg])))

class _summary_activity:
    def __init__(self):
        self._tables = {}

    def add_table(self, table_name):
        # self._tables.append(table_name);
        self._tables[table_name.upper()]=0

    def update_record(self, table_name, record_no):
        # self._tables.append(table_name);
        self._tables[table_name.upper()]=record_no

    def list_table(self):
        for table in self._tables:
            log.info(" table : %s = %d record(s)" % (table,self._tables[table]))

# TBD
class _dynamic_sql:
    def __init__(self,db_typ):

        
        self._var_scanlar = {}

        # self._dbconect = _db()

        if   db_typ == "DB_Vertica" :
            self.var_config = Varlib._var_db_vertica()
        elif db_typ == "DB_MySQL" :
            self.var_config = Varlib._var_db_mysql()
        elif db_typ == "DB_Impala" :
            self.var_config = Varlib._var_db_impala()            
        else:
            log.error("ERROR Unknow DB TYPE : %s" % db_typ)
            sys.exit(2)        

        pass

    def __del__(self):

        del self._var_scanlar
        # del self._dbconect
        del self.var_config

        pass

    def add_scalar(self,variable_name,value):
        self._var_scanlar[variable_name] = value

    def get_scalar(self,variable_name):

        if variable_name in self._var_scanlar.keys():        
            return self._var_scanlar[variable_name]
        else:
            print(f'Error not found variable : {variable_name}')
            sys.exit(1)

    def build_sql_dynamic(self,sql_variable):
        sql_master = sql_variable
        
        log.debug(f'build_sql_dynamic : {sql_master}')

        sql_build = sql_master
        match  = sorted(set(re.findall('\{[\w:]*\}',sql_master.replace('{{','[['))))
        for list in match:
            
            # log.info(f'{list}')
            var_name = list.replace('{','').replace('}','')

            if ':' in var_name:
                log.debug(f'{var_name} is build by function')

                (type_function, variable) = var_name.split(':')

                # Support function : DB                
                if type_function.strip().upper() == "DB":

                    str_eval = f'self.var_config.{variable}()'
                    log.debug(f'str_eval : {str_eval}')
                    
                    str_sql = eval(str_eval)

                    log.debug(f'str_sql : {str_sql}')

                    # call build again
                    str_sql = self.build_sql_dynamic(str_sql)

                    log.debug(f'str_sql : {str_sql}')
                    
                    BTable = self._dbconect.query_DB(str_sql)
                    log.debug(f'\n{BTable}')                    

                    result_sql = BTable.rows[0][0] if BTable.rows[0][0] else "1 = 0"

                    log.debug(f'result_sql : {result_sql}')

                    sql_build = sql_build.replace('{' + var_name + '}',result_sql.upper())

                elif type_function.strip().upper() == "LIST":
                    # log.info(f'---------------------------------------------------------------')
                    # log.info(f'found list: {type_function} : {variable}')
                    # log.info(f'---------------------------------------------------------------')

                    # TBD_Value = ['\'LINE_NO\'','\'1\'']
                    sql_build = sql_build.replace('{' + var_name + '}',self.get_scalar(var_name))


                # self._dbconect.query_DB()
            else:
                log.debug(f'{var_name} is scala variable')

                sql_build = sql_build.replace('{' + var_name + '}',self.get_scalar(var_name))

        log.debug(f'sql_build : {sql_build}')

        return sql_build
