#!/usr/local/bin/python3
##################################################################################################
# SCRIPT NAME      : db_build.py
# DESCRIPTION      : propose for build for change schema variable list files
#                      - {tablename}_data.sql
#                      - {tablename}_ddl.sql
#                    on Database build stage
# AUTHOR           : Suebsakul Ampa
# CREATED DATE     : 03/11/2021
# USAGE            : ./db_build.sh {ENV} {DB_PATH} {SQL_TYPE}
#-------------------------------------------------------------------------------------------------
# CHANGED REVISION
#
# UPDATED BY       : Suebsakul Ampa
# UPDATED DATE     : 03/11/2021
# Initial version
#
# UPDATED BY       : 
# UPDATED DATE     : 
#
##################################################################################################

import pandas as pd
import openpyxl
import logging
import sys, getopt
import os, shutil
import re
from datetime import datetime
import time
import glob

from pathlib import Path
#module sql statment
import sqlparse
import sql_metadata

#module DB
import mariadb

import uuid
import difflib
from difflib import Differ


#change directory
# only debug mode
# os.chdir('D:\\Workspace\\TOP_Crude_Flex\\Monitor\\etl\\bin\\')
PWD = os.getcwd()

CONF_RULES = '/CodeDeploy/util/util_config.xlsx'

os.chdir('D:\\Workspace\\Auto_Deploy\\CodeDeploy\\' if "win" in sys.platform else PWD+'/CodeDeploy/')

# insert at 1, 0 is the script path (or '' in REPL)
# sys.path.insert(0, '../libs')
import python_libs as Mylib
import folder_libs as Folderlib

# sys.path.insert(0, '..')
# from libs import python_libs as Mylib
# from libs import folder_libs as Folderlib

#----------------------------------------- Variable ------------------------------------------
# define veriable
Mylib.EXEC_PARA='-e <ENV Develop, QA or Production> -p <Root DB Path> -t <Type DDL or DATA>'
DB_CHAR  = "utf8"

folder_lib = Folderlib._folder()


if len(sys.argv) <= 1 :
    Mylib._usage(sys.argv)

commandline_argment = Mylib._commandline_argment()

try:
    opts, args = getopt.getopt(sys.argv[1:],"he:p:t:",["env=","path=","type="])

    for (o, v) in opts:
        if o == '-e':
            # file_load = v
            commandline_argment.add('env',v)
        elif o == '-p':
            commandline_argment.add('db_path',v)
        elif o == '-t':
            commandline_argment.add('sql_type',v)
        elif o == '-h':
            Mylib._usage(sys.argv)
        else:
            # sys.stderr.write("%s: illegal option '%s'\n" % (args[0], o))
            # log.error("%s: illegal option '%s'\n" % (args[0], o))
            Mylib._usage(sys.argv)
except:
    Mylib._usage(sys.argv)

# #assign default argment
# if commandline_argment.get('Mode') == None:
#     commandline_argment.add('Mode','default')

#assign uuid.uuid1()
UUID = str(uuid.uuid1())
commandline_argment.add('uuid',UUID)
#----------------------------------------- Function ------------------------------------------

#TBD
def check_newline_file(file):
    # CRLF : \r\n
    # LF   : \n
    with open(file,mode="r",encoding="utf-8", errors='ignore') as f:
        for line in f:
            if re.match('\r\n$',str(line)):
                commandline_argment.add('NewLine','CRLF')
                break;
            else:
                commandline_argment.add('NewLine','LF')
                break;

def load_rules_replaces(file_rules_replace):

    df = pd.read_excel(file_rules_replace, sheet_name='build__db_schema')

    # Filter only active and same DB Path
    df_rules_replace = df[(df.Status.str.lower() == 'Active'.lower()) & (df.DB_PATH.str.lower() == commandline_argment.get('DB_PATH').lower())]
    log.info('Filename : %s on Sheet: = %d record(s)' % (file_rules_replace, len(df_rules_replace)))                                

    return df_rules_replace


def rules_replaces_exec(filename_sql, df_rules_replace):  

    try:
        f = open(filename_sql, mode="r", encoding="utf-8")
        file_content_original = f.read()            
        f.close()        
        file_content_replaced = file_content_original

        for index, row in df_rules_replace.iterrows():
            log.info('Replace Rule[%2d] : %s -> %s ' % (index+1, row['Find'], row[commandline_argment.get('ENV')]))

            # support reserve charecter of regular
            row['Find'] = row['Find'].replace("$","\$")

        #     row['Find'] = row['Find'].replace("(","\(").replace(")","\)")
        #     row['Find'] = row['Find'].replace("{","\{").replace("}","\}")            
        #     row['Find'] = row['Find'].replace("|","\|")
        #     row['Find'] = row['Find'].replace("+","\+")
        #     row['Find'] = row['Find'].replace("<>","\<\>")

        #     # decode to group of word of regular
        #     row['Find'] = row['Find'].replace("<[","([").replace("]*>","]*)")

            # matches = re.search("("+row['Find']+")", file_content_replaced, re.IGNORECASE)
            matches = re.search("("+row['Find']+")", file_content_replaced, re.IGNORECASE)

            if matches:
                file_content_replaced = re.sub(row[r'Find'],row[commandline_argment.get('ENV')],file_content_replaced)
                time.sleep(0.5)

        #Show diff
        diff_count=0
        for line in difflib.unified_diff(
                                        file_content_original.splitlines(keepends=True),
                                        file_content_replaced.splitlines(keepends=True),
                                        fromfile='before', tofile='after'):            

            if line.startswith('+') or line.startswith('-'):
                diff_count = diff_count + 1
                log.info(line.replace('\n',''))
        
        # check should be diff count more than 0
        if diff_count <= 0:
            log.error("not found diff record on file : %s" % file_sql)
            sys.exit(2)
        else:
            log.info("pass found diff : %d line(s) on file : %s" % (diff_count, file_sql))

        #write to path replace
        fw = open(filename_sql, encoding="utf-8" , mode="w+")
        fw.write(file_content_replaced)
        fw.close()
        pass

    except:
        raise("Can't replace file: %s" % filename_sql)
#------------------------------------------- Main --------------------------------------------

# define loggin
start_time = time.time()
log = logging.getLogger() # 'root' Logger

# logging.basicConfig(filename='myapp.log', level=logging.INFO, format='[%(asctime)s] {%(filename)s:%(lineno)d} %(levelname)s - %(message)s')
log.info("start")
log.info("log level : %s" % logging.getLevelName(log.level))

# list argments
commandline_argment.list()

try:

    # list file.sql follow condition below
    # <DB_PATH>\<SCHEMA>\ddl\<tablename>_ddl.sql
    # <DB_PATH>\<SCHEMA>\data\<tablename>_data.sql    
  
    log.info("List files on path: "+folder_lib.cnv_path(PWD +'/'+ commandline_argment.get('DB_PATH') +
                                                       '/*/'+ commandline_argment.get('SQL_TYPE')+
                                                       '/*_'+ commandline_argment.get('SQL_TYPE')+'.sql'))

    
    list_file_load = glob.iglob(folder_lib.cnv_path(PWD +'/'+ commandline_argment.get('DB_PATH') +
                                                   '/*/'+ commandline_argment.get('SQL_TYPE')+
                                                   '/*_'+ commandline_argment.get('SQL_TYPE')+'.sql'),recursive = True)
    
    # load rule replace
    log.info("load rules replace..")
    df_rules_replace = load_rules_replaces(PWD + CONF_RULES)

    count=0
    for file_sql in list_file_load:                
        count = count + 1
        log.info("found file %d : %s" % (count, file_sql))

        if os.access(file_sql, os.R_OK) == False:    
            log.error("can't access read file : %s" % file_sql)
            sys.exit(2)

        rules_replaces_exec(file_sql,df_rules_replace)

    log.info("total %d file(s)" % count)

    log.info("List filesname wrong format on path: "+folder_lib.cnv_path(PWD +'/'+ commandline_argment.get('DB_PATH') +
                                                   '/*/'+ commandline_argment.get('SQL_TYPE')+
                                                   '/*.sql'))

    list_all_file_load = glob.iglob(folder_lib.cnv_path(PWD +'/'+ commandline_argment.get('DB_PATH') +
                                                   '/*/'+ commandline_argment.get('SQL_TYPE')+
                                                   '/*.sql'),recursive = True)

    count_wrong=0
    for filename in list_all_file_load:
        if (not filename.lower().endswith("_data.sql") and not filename.lower().endswith("_ddl.sql")) or \
           (re.search("/data/.*_ddl.sql$",filename) or re.search("/ddl/.*_data.sql$",filename)) :
            count_wrong = count_wrong + 1
            log.info("found file %d : %s" % (count_wrong, filename))

    if count_wrong > 0 :
        log.error('ERROR found filename wrong format')      
        sys.exit(2)

    log.info("done")

except Exception as e:
    print(f"Error: {e}")  
    log.error(f'Error: {e}')      
    sys.exit(2)
