
class _var_db_vertica:

    def SQL_CHK_TABLE(self):
        return  """
                    SELECT
                        COUNT(*) NUMBER_OF_RECORD
                    FROM
                        V_CATALOG.TABLES T
                    WHERE
                        T.TABLE_NAME = '{TABLE_NAME}'
                        AND T.TABLE_SCHEMA = '{SCHEMA}';
                """

    def SQL_RENAME_TABLE(self):
        return  """
                   ALTER TABLE {SCHEMA}.{TABLE_NAME} RENAME TO ZBK_{TABLE_NAME}_{BUILD_NUMBER}_{DATETIME};
                """

    def SQL_ROLLBACK_TABLE(self):
        return  """
                   ALTER TABLE {SCHEMA}.ZBK_{TABLE_NAME}_{BUILD_NUMBER}_{DATETIME} RENAME TO {TABLE_NAME};
                """                

class _var_db_mysql:
    
    def SQL_CHK_TABLE(self):
        return  """
                    SELECT
                        COUNT(*) NUMBER_OF_RECORD
                    FROM
                        INFORMATION_SCHEMA.TABLES T
                    WHERE
                        T.TABLE_NAME = '{TABLE_NAME}'
                        AND T.TABLE_SCHEMA = '{SCHEMA}';
                """

    def SQL_GET_LAST_BAK_TABLE(self):
        return  """
                    SELECT
                        TABLE_NAME
                    FROM
                        INFORMATION_SCHEMA.TABLES T
                    WHERE
                        T.TABLE_NAME LIKE 'ZBK_{TABLE_NAME}_{BUILD_NUMBER}_%'
                        AND T.TABLE_SCHEMA = '{SCHEMA}'
                    ORDER BY TABLE_NAME DESC LIMIT 1;
                """

    def SQL_GET_NUMBER_OF_RECORD(self):
        return  """
                    SELECT
                        count(*) NUMBER_OF_RECORD
                    FROM
                        {SCHEMA}.{TABLE_NAME};
                """                

    def SQL_CLONE_TABLE(self):
        return  """
                   CREATE TABLE {SCHEMA}.ZBK_{TABLE_NAME}_{BUILD_NUMBER}_{DATETIME} LIKE {SCHEMA}.{TABLE_NAME};
                """                

    def SQL_RENAME_TABLE(self):
        return  """
                   ALTER TABLE {SCHEMA}.{TABLE_NAME} RENAME TO {SCHEMA}.ZBK_{TABLE_NAME}_{BUILD_NUMBER}_{DATETIME};
                """                

    def SQL_ROLLBACK_TABLE_CURR_TO_ROLL(self):
        return  """
                   ALTER TABLE {SCHEMA}.{TABLE_NAME} RENAME TO {SCHEMA}.ZROLL_{TABLE_NAME}_{BUILD_NUMBER}_{DATETIME};
                """                
    def SQL_ROLLBACK_TABLE_BAK_TO_CURR(self):
        return  """
                   ALTER TABLE {SCHEMA}.{TABLE_BACKUP} RENAME TO {SCHEMA}.{TABLE_NAME};                   
                """                

class _var_db_impala:
    
    def SQL_CHK_TABLE(self):
        return  """
                    SELECT
                        COUNT(*) NUMBER_OF_RECORD
                    FROM
                        V_CATALOG.TABLES T
                    WHERE
                        T.TABLE_NAME = '{TABLE_NAME}'
                        AND T.TABLE_SCHEMA = '{SCHEMA}';
                """

    def SQL_GET_NUMBER_OF_RECORD(self):
        return  """
                    SELECT
                        count(*) NUMBER_OF_RECORD
                    FROM
                        {SCHEMA}.{TABLE_NAME};
                """

    def SQL_RENAME_TABLE(self):
        return  """
                   ALTER TABLE {SCHEMA}.{TABLE_NAME} RENAME TO ZBK_{TABLE_NAME}_{BUILD_NUMBER}_{DATETIME};
                """                

    def SQL_ROLLBACK_TABLE(self):
        return  """
                   ALTER TABLE {SCHEMA}.ZBK_{TABLE_NAME}_{BUILD_NUMBER}_{DATETIME} RENAME TO {TABLE_NAME};
                """                
