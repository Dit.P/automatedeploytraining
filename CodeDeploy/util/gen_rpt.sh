#!/bin/bash
# Testing
# cat git_diff.txt | ./gen_rpt.sh deploy_result.txt report.html CR20001 15

#***********************************************
# Conguration Mapping field
#format => [GIT_ACTION]:[GIT_DEPLOY]:[GIT_ROLLBACK]
#Example => M:Modify:Replace Old version

GIT_ACTION_MAP=( "A:Add file:Use the previous version on Git" "M:Add/Modify file:Use the previous version on Git" "D:Delete file:Use the previous version on Git" )
DDL_ACTION_MAP=( "A:Create Table:Replace with backup table" "M:Create Table:Replace with backup table" "D:No deploy:No roll back" )
DATA_ACTION_MAP=( "A:Insert/Update/Delete Data:Replace with backup table" "M:Insert/Update/Delete Data:Replace with backup table" "D:No deploy:No roll back" )
#***********************************************
git_map() {
   for l_map in "${GIT_ACTION_MAP[@]}" ; do
      KEY=${l_map%%:*}
      VALUE=${l_map#*:}
      if [ ${KEY} == $1 ]; then
         #echo $VALUE
	 #echo $VALUE | awk 'BEGIN { FS="[:]" } ; { print $2 }'
	 if [ $2 -eq 1 ]; then
	    echo $VALUE | cut -d ":" -f1
	 elif [ $2 -eq 2 ]; then
	    echo $VALUE | cut -d ":" -f2
	 fi
      fi
   done
}
ddl_map() {
   for l_map in "${DDL_ACTION_MAP[@]}" ; do
      KEY=${l_map%%:*}
      VALUE=${l_map#*:}
      if [ ${KEY} == $1 ]; then
         #echo $VALUE
	 #echo $VALUE | awk 'BEGIN { FS="[:]" } ; { print $2 }'
	 if [ $2 -eq 1 ]; then
	    echo $VALUE | cut -d ":" -f1
	 elif [ $2 -eq 2 ]; then
	    echo $VALUE | cut -d ":" -f2
	 fi
      fi
   done
}
data_map() {
   for l_map in "${DATA_ACTION_MAP[@]}" ; do
      KEY=${l_map%%:*}
      VALUE=${l_map#*:}
      if [ ${KEY} == $1 ]; then
         #echo $VALUE
	 #echo $VALUE | awk 'BEGIN { FS="[:]" } ; { print $2 }'
	 if [ $2 -eq 1 ]; then
	    echo $VALUE | cut -d ":" -f1
	 elif [ $2 -eq 2 ]; then
	    echo $VALUE | cut -d ":" -f2
	 fi
      fi
   done
}
#***********************************************

declare -a git_action
declare -a git_file
tmp_read=""
NEWLINE=$'\n'
template_file="CodeDeploy/util/rpt_template/html_template.html"
git_section_file="tmp_git_section.html"
item_section_file="tmp_item_section.html"

total_files=0
total_success=0
toal_fail=0
if [ -p /dev/stdin ]; then
   if ! [[ "$#" -eq 4 ]]; then
       echo "Invalid arguments!"
       echo "Ex. .... | ./gen_rpt.sh deploy_result.txt outputpath/report.html feature_name build_number"
       exit 1;
   fi
   input_deploy_result_file=$1
   reporthtml_file_path=$2
   feature_name=$3
   build_number=$4
   while IFS= read line; do
      tmp_read+="${line}${NEWLINE}"
   done
   readarray -t strarr <<<"$tmp_read"
   for (( n=0; n < ${#strarr[*]}; n++ ))
   do
      #Ignore all line have content start with ':'
      if ! [[ ${strarr[n]} == :* ]] && ! [[ -z ${strarr[n]} ]]; then
	 git_action[${#git_action[@]}]=`echo ${strarr[n]} | awk 'BEGIN { FS="[ ]" } ; { print $1 }'`
	 git_file[${#git_file[@]}]=`echo ${strarr[n]} | awk 'BEGIN { FS="[ ]" } ; { print $2 }'`
      fi
   done
   #echo "array length = ${#git_a[*]}"
   #*** If all files is extracted properly. So, Length of array must greater than zero***
   if [ ${#git_action[*]} -gt 0 ]; then
      
      #*** Generate : GIT Section **********************************
      #** Prepare step, Delete a temporary file before generate ****
      if [[ -f $git_section_file ]]; then
         rm -f $git_section_file
      fi
      touch $git_section_file
      #*************************************************************
      for (( i=0; i < ${#git_action[*]}; i++ ))
      do
         if [[ ${git_file[i]} =~ .*_ddl.sql.* ]]; then
            #echo "$(($i+1)) - ${git_action[i]} - [ $(ddl_map ${git_action[i]} 1) ] - [ $(ddl_map ${git_action[i]} 2) ] - ${git_file[i]}"
	    echo "<tr class=xl65 height=18 style=\"height:13.8pt\"><td height=18 class=xl65 style=\"height:13.8pt\"></td><td class=xl71 style=\"border-top:none\">$(($i+1))</td><td colspan=3 class=xl78 style=\"border-right:.5pt solid black;border-left:none\">${git_file[i]}</td><td class=xl70 style=\"border-top:none;border-left:none\">${git_action[i]}</td><td class=xl70 style=\"border-top:none;border-left:none\">$(ddl_map ${git_action[i]} 1)</td><td class=xl70 style=\"border-top:none;border-left:none\">$(ddl_map ${git_action[i]} 2)</td><td class=xl65></td></tr>" >> $git_section_file
	 elif [[ ${git_file[i]} =~ .*_data.sql.* ]]; then
	    #echo "$(($i+1)) - ${git_action[i]} - [ $(data_map ${git_action[i]} 1) ] - [ $(data_map ${git_action[i]} 2) ] - ${git_file[i]}"
	    echo "<tr class=xl65 height=18 style=\"height:13.8pt\"><td height=18 class=xl65 style=\"height:13.8pt\"></td><td class=xl71 style=\"border-top:none\">$(($i+1))</td><td colspan=3 class=xl78 style=\"border-right:.5pt solid black;border-left:none\">${git_file[i]}</td><td class=xl70 style=\"border-top:none;border-left:none\">${git_action[i]}</td><td class=xl70 style=\"border-top:none;border-left:none\">$(data_map ${git_action[i]} 1)</td><td class=xl70 style=\"border-top:none;border-left:none\">$(data_map ${git_action[i]} 2)</td><td class=xl65></td></tr>" >> $git_section_file
	 else
	    #echo "$(($i+1)) - ${git_action[i]} - [ $(git_map ${git_action[i]} 1) ] - [ $(git_map ${git_action[i]} 2) ] - ${git_file[i]}"
	    echo "<tr class=xl65 height=18 style=\"height:13.8pt\"><td height=18 class=xl65 style=\"height:13.8pt\"></td><td class=xl71 style=\"border-top:none\">$(($i+1))</td><td colspan=3 class=xl78 style=\"border-right:.5pt solid black;border-left:none\">${git_file[i]}</td><td class=xl70 style=\"border-top:none;border-left:none\">${git_action[i]}</td><td class=xl70 style=\"border-top:none;border-left:none\">$(git_map ${git_action[i]} 1)</td><td class=xl70 style=\"border-top:none;border-left:none\">$(git_map ${git_action[i]} 2)</td><td class=xl65></td></tr>" >> $git_section_file
	 fi
      done
      #*** End of Generate GIT Section *****************************

      #** Generate : ITEM Section **********************************
      # Input argument have to provided !!
      #** Prepare step, Delete a temporary file before generate ****
      if [[ -f $item_section_file ]]; then
         rm -f $item_section_file
      fi
      touch $item_section_file
      #*************************************************************
      while IFS= read -r line; do
         #echo "Reading -> $line"
         item_name=`echo $line | awk 'BEGIN { FS="[|]" } ; { print $1 }'`
         item_total=$((`echo $line | awk 'BEGIN { FS="[|]" } ; { print $2 }'`))
         item_fail=$((`echo $line | awk 'BEGIN { FS="[|]" } ; { print $3 }'`))
         total_files=$((total_files+item_total))
         total_fail=$((total_fail+item_fail))

	 echo "<tr height=19 style='height:14.4pt'><td height=19 style='height:14.4pt'></td><td colspan=5 class=xl72>${item_name}</td><td class=xl68 align=right style='border-top:none;border-left:none'>${item_total}</td><td class=xl68 align=right style='border-top:none;border-left:none'>${item_fail}</td><td></td></tr>" >> $item_section_file

      done < ${input_deploy_result_file}
      total_success=$((total_files - total_fail))
      echo "total_files = ${total_files}"
      echo "total_fail = ${total_fail}"
      echo "total_success = ${total_success}"
      #** End of Generate ITEM Section *****************************

      #Generate Final Report
      rpt_datetime=`date '+%A,%e %B %Y %r'`;
      cp ${template_file} ${reporthtml_file_path}_pre

      sed -i -e "s/%FEATURE_NAME%/${feature_name}/g" -e "s/%BUILD_NUMBER%/${build_number}/g" -e "s/%REPORT_TIMESTAMP%/${rpt_datetime}/g" ${reporthtml_file_path}_pre
      sed -i -e "s/%TOTAL_DEPLOY%/${total_files}/g" -e "s/%TOTAL_FAIL%/${total_fail}/g" -e "s/%TOTAL_SUCCESS%/${total_success}/g" ${reporthtml_file_path}_pre
      if [ ${total_fail} -gt 0 ]; then
         sed -i -e "s/%FCLASS%/failColor/g" ${reporthtml_file_path}_pre
      else
         sed -i -e "s/%FCLASS%/passColor/g" ${reporthtml_file_path}_pre
      fi
      
      var_git_info=`cat ${git_section_file}`
      var_item_info=`cat ${item_section_file}`
      while IFS= read -r line; do
         if [[ ${line} =~ .*GIT_INFO_SECTION.* ]]; then
	    echo ${var_git_info}
	 elif [[ ${line} =~ .*ITEM_INFO_SECTION.* ]]; then
	    echo ${var_item_info}
	 else
	    echo ${line}
	 fi
      done < ${reporthtml_file_path}_pre > ${reporthtml_file_path}
      rm -f ${reporthtml_file_path}_pre
      rm -f ${git_section_file}
      rm -f ${item_section_file}
      echo "generate report completed"
   fi
else
   echo "No datas on stdin, skipping!"
fi
