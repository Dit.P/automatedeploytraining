import pandas as pd
import openpyxl
import logging
import logging.handlers
import sys, getopt
import os
import re
from datetime import datetime
# import time
# import glob
import __main__
from beautifultable import BeautifulTable
#from termcolor import colored

# import variable_libs     as Varlib
# import variable_new_libs as VarNewlib
import inspect

#----------------------------------------- Logging ------------------------------------------
log = logging.getLogger()

#----------------------------------------- Function ------------------------------------------
class _folder:
    def __init__(self):
        self._arg = {}

        self._base   = "D:/Workspace/Auto_Deploy" if "win" in sys.platform else "/dpo/TOP_CWP/crude_flex"
        self._root   = "D:/Workspace/Auto_Deploy" if "win" in sys.platform else "/home/aepsrv1/workspace/feature/feature_99_develop"
        self._mysql   = "DB_MySQL"
        self._vertica = "DB_Vertica"
        self._impala  = "DB_Impala"

        # self._output = "output"
        # self._logs   = "logs"
        # self._archive= "archive"

        # self._sourceTemp = "source/temp"


        # self._homeETL= "Monitor/etl"
        # self._design = "Monitor/etl/design"
        # self._bin    = "Monitor/etl/bin"
        # self._libs   = "Monitor/etl/libs"


    def cnv_path(self, in_path):
        
        out_path = os.path.abspath(in_path)

        if not os.access(out_path, os.F_OK) and out_path.find("*") == 0:
            print(f'Error : Can not access {out_path}')
            sys.exit(2)

        if not os.access(out_path, os.R_OK) and out_path.find("*") == 0:
            print(f'Error : Can not read {out_path}')
            sys.exit(2)

        if not os.access(out_path, os.W_OK) and out_path.find("*") == 0:
            print(f'Error : Can not write {out_path}')
            sys.exit(2)          

        return out_path

    def get(self, argment_name):
        
        str_return = ""

        if argment_name.lower() == "db_mysql":
            str_return = self._root + "/" + self._mysql
        elif argment_name.lower() == "db_vertica":
            str_return = self._root + "/" + self._vertica
        elif argment_name.lower() == "db_impala":
            str_return = self._root + "/" + self._impala
        elif argment_name.lower() == "logs":
            str_return = self._root + "/" + self._logs            
        # elif argment_name.lower() == "archive":
        #     str_return = self._root + "/" + self._archive

        # elif argment_name.lower() == "sourcetemp":
        #     str_return = self._root + "/" + self._sourceTemp            

        # elif argment_name.lower() == "homeetl":
        #     str_return = self._base + "/" + self._homeETL
        # elif argment_name.lower() == "design":
        #     str_return = self._base + "/" + self._design            
        # elif argment_name.lower() == "bin":
        #     str_return = self._base + "/" + self._bin
        # elif argment_name.lower() == "libs":
        #     str_return = self._base + "/" + self._libs

        else:
            print(f'Error : Unknow type of folder {argment_name}')
            sys.exit(2)

        var_return = os.path.abspath(str_return)

        if not os.access(var_return, os.F_OK) :
            print(f'Error : Can not access {var_return}')
            sys.exit(2)

        if not os.access(var_return, os.R_OK) :
            print(f'Error : Can not read {var_return}')
            sys.exit(2)

        if not os.access(var_return, os.W_OK) :
            print(f'Error : Can not write {var_return}')
            sys.exit(2)        

        return var_return
