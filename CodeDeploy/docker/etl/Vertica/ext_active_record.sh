#!/bin/bash
echo "hello planet argument: $1" >> /tmp/myprocedure.log 


if   [ "$1" == ".IF ACTIVITYCOUNT > 1"  ];
then
    echo ".IF ACTIVITYCOUNT > 1"
elif [ "$1" == ".IF ACTIVITYCOUNT = 0"  ];
then
    echo ".IF ACTIVITYCOUNT = 1"
elif [ "$1" == ".IF ACTIVITYCOUNT <> 0" ];
then
    echo ".IF ACTIVITYCOUNT <> 0"
else
    exit 1
fi
