-- create the databases
-- CREATE DATABASE IF NOT EXISTS DPO_REPO;
-- DEFAULT CHARACTER SET utf8
-- DEFAULT COLLATE utf8_general_ci;
-- create the users for each database
-- CREATE USER 'projectoneuser'@'%' IDENTIFIED BY 'somepassword';
-- GRANT CREATE, ALTER, INDEX, LOCK TABLES, REFERENCES, UPDATE, DELETE, DROP, SELECT, INSERT ON `projectone`.* TO 'projectoneuser'@'%';

CREATE SCHEMA IF NOT EXISTS AEP_1;
CREATE SCHEMA IF NOT EXISTS aepdev;


CREATE TABLE AEP_1.aep_context_t (
	region varchar(100) NULL,
	ctxarea varchar(100) NULL,
	ctxjob varchar(100) NULL,
	ctxkey varchar(100) NULL,
	ctxval varchar(100) NULL,
	remarks varchar(100) NULL,
	CONSTRAINT aep_context_t_PK PRIMARY KEY (region,ctxarea,ctxjob,ctxkey)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb3
COLLATE=utf8mb3_general_ci;

CREATE TABLE AEP_1.aep_watcher_tbl (
	SSD varchar(100) NULL,
	SSE varchar(100) NULL,
	count INT NULL,
	Flag varchar(100) NULL,
	Schedule_count INT NOT NULL,
	CONSTRAINT aep_watcher_tbl_PK PRIMARY KEY (SSD,SSE)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb3
COLLATE=utf8mb3_general_ci;

CREATE TABLE AEP_1.m_elt_properties_feed (
  feed_name varchar(100) NOT NULL,
  prop_name varchar(100) NOT NULL,
  prop_value varchar(100) DEFAULT NULL,
  flag varchar(100) DEFAULT NULL,
  create_by varchar(100) DEFAULT NULL,
  create_date datetime DEFAULT NULL,
  update_by varchar(100) DEFAULT NULL,
  update_date datetime DEFAULT NULL,
  PRIMARY KEY (feed_name,prop_name)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3

-- select 'drop table ' || table_schema || '.' || table_name || ' cascade;' from v_catalog.tables t where t.table_name like 'ZBK%';
-- FLUSH PRIVILEGES;
