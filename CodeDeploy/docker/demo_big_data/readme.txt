
docker-compose -f .\docker-compose_dpo_automate_deploy.yml up -d

docker volume prune -f

# a fresh start for everything
docker system prune -a
