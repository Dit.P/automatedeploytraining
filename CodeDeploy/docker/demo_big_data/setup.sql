-- create the databases
CREATE DATABASE IF NOT EXISTS DPO_REPO;
DEFAULT CHARACTER SET utf8
DEFAULT COLLATE utf8_general_ci;
-- create the users for each database
-- CREATE USER 'projectoneuser'@'%' IDENTIFIED BY 'somepassword';
-- GRANT CREATE, ALTER, INDEX, LOCK TABLES, REFERENCES, UPDATE, DELETE, DROP, SELECT, INSERT ON `projectone`.* TO 'projectoneuser'@'%';

-- FLUSH PRIVILEGES;
