-- create the databases
-- CREATE DATABASE IF NOT EXISTS DPO_REPO;
-- DEFAULT CHARACTER SET utf8
-- DEFAULT COLLATE utf8_general_ci;
-- create the users for each database
-- CREATE USER 'projectoneuser'@'%' IDENTIFIED BY 'somepassword';
-- GRANT CREATE, ALTER, INDEX, LOCK TABLES, REFERENCES, UPDATE, DELETE, DROP, SELECT, INSERT ON `projectone`.* TO 'projectoneuser'@'%';

CREATE SCHEMA IF NOT EXISTS DTDVTA;
CREATE SCHEMA IF NOT EXISTS DTDVTB;
CREATE SCHEMA IF NOT EXISTS DTDVTC;
CREATE SCHEMA IF NOT EXISTS DTDVTO;
CREATE SCHEMA IF NOT EXISTS DTDVTL;

-- select 'drop table ' || table_schema || '.' || table_name || ' cascade;' from v_catalog.tables t where t.table_name like 'ZBK%';
-- FLUSH PRIVILEGES;
