
CREATE TABLE ${DB_TRG}.CCB_CS_DUAL_MAST_AGING
(
    CUST_NUMB           INT NOT NULL,
    SUBR_NUMB           VARCHAR(12) NOT NULL,
    SCND_CUST_NUMB      INT NOT NULL,
    SCND_TITL           VARCHAR(160),
    SCND_FRST_NAME      VARCHAR(250) NOT NULL,
    SCND_LAST_NAME      VARCHAR(250),
    SCND_LANG           VARCHAR(4) NOT NULL,
    CRTD_DTTM           TIMESTAMP,
    CRTD_BY             VARCHAR(48) NOT NULL,
    LAST_CHNG_DTTM      TIMESTAMP,
    LAST_CHNG_BY        VARCHAR(48) NOT NULL,
    SBSCRPN_ID          INT,
    BUSINESS_DATE       DATE,
    EXECUTION_ID        INT,
    PROCESS_NAME        VARCHAR(600),
    DW_LAST_UPDATE_TIME TIMESTAMP(0)
);
