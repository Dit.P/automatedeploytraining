
CREATE TABLE ${DB_TRG}.CBS_AR_INVOICE_AGING 
(
    BACKUP_MNTH_ID      INTEGER NOT NULL,
    LAST_C7_FLAG        VARCHAR(5) ,
    INVOICE_ID          NUMERIC(22,2) NOT NULL,
    COMP_CD             VARCHAR(20) NOT NULL,
    INVOICE_NO          VARCHAR(336),
    PARENT_INVOICE_ID   NUMERIC(22,2),
    TRANS_TYPE          VARCHAR(112) NOT NULL,
    CHANNEL_ID          VARCHAR(176),
    ACCT_ID             NUMERIC(22,2) NOT NULL,
    PRI_IDENTITY        VARCHAR(84),
    INVOICE_AMT         NUMERIC(22,2) NOT NULL,
    OPEN_AMT            NUMERIC(22,2) NOT NULL,
    INVOICE_DATE        TIMESTAMP,
    DUE_DATE            DATE,
    INVOICE_STATUS      VARCHAR(84) NOT NULL,
    BILL_CYCLE_ID       VARCHAR(120),
    TRANS_ID            NUMERIC(22,2),
    TAX_AMT             NUMERIC(22,2),
    DISPUTE_AMT         NUMERIC(22,2),
    DISPUTE_TAX_AMT     NUMERIC(22,2),
    PENDING_CR_AMT      NUMERIC(22,2),
    AOD_STATUS          VARCHAR(84),
    CLOSE_DATE          TIMESTAMP,
    REASON_CODE         VARCHAR(144),
    REMARK              VARCHAR(592),
    OPER_ID             VARCHAR(336) NOT NULL,
    DEPT_ID             VARCHAR(336) NOT NULL,
    LAST_UPDATE_DATE    TIMESTAMP,
    ENTRY_DATE          TIMESTAMP,
    OPEN_TAX_AMT        NUMERIC(22,2),
    INVOICE_TYPE        VARCHAR(112) NOT NULL,
    EXT_PROPERTY        VARCHAR(16080),
    WHTINVOICEFLAG      VARCHAR(1),
    BUSINESS_DATE	    DATE,
	EXECUTION_ID	    INTEGER,
	PROCESS_NAME 	    VARCHAR(600),
	DW_LAST_UPDATE_TIME	TIMESTAMP(0)
);
