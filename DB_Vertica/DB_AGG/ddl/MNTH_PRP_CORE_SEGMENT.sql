
DROP TABLE IF EXISTS DTPVTT.VOL_PROFL;
CREATE TABLE DTPVTT.VOL_PROFL as(
SELECT SUBR_ID
FROM DTPVTA.SNP_MNTH_SUBR_PROFL
    WHERE MNTH_ID =  202110
    AND ACCT_TYPE_CD = 0
    AND CCB_STAT_CD IN ('A', 'S')
    AND SBSCRPN_CLASS_CD in ('N')
    AND ACCT_CLASS_CD  = 'I' 
    AND OUTG_USR_FLG = 'Y'
    AND SUBR_PROFL_TYPE = 'EOM'
    group by 1
)SEGMENTED BY HASH(subr_id) ALL NODES;

--- Topping
DROP TABLE IF EXISTS DTPVTT.VOL_TOPPING ;
CREATE TABLE DTPVTT.VOL_TOPPING AS ( 
SELECT  SUBR_ID 
       ,count(*) TOPPING_CNT
FROM DTPVTA.FCT_DAIL_TOPPING
WHERE DT BETWEEN date '2021-10-31'-45 AND DATE '2021-10-31'
GROUP BY 1
)SEGMENTED BY HASH(subr_id) ALL NODES;


-- Revenue
DROP TABLE IF EXISTS DTPVTT.VOL_REV;
CREATE TABLE DTPVTT.VOL_REV as(
SELECT
	  SUBR_ID
	, SUM(REVN_AF_SHAR) AS REV_AF_SHAR
FROM DTPVTA.CLSTR_DAIL_SUBR_PP_REVN_TTL
WHERE  DAY_ID between date '2021-10-01' and date '2021-10-31'
GROUP BY 1
)SEGMENTED BY HASH(subr_id) ALL NODES;


drop table if exists DTPVTT.VOL_REV_PCT ;
CREATE TABLE DTPVTT.VOL_REV_PCT as(
	SELECT SUBR_ID , ORD , ORD/cnt_all AS PCT ,REV_AF_SHAR_AMT
	from (
		SELECT A1.subr_id ,ROW_NUMBER() OVER(ORDER BY REV_AF_SHAR_AMT desc) ord ,REV_AF_SHAR_AMT
		from (
			SELECT A0.SUBR_ID
			,case when B0.REV_AF_SHAR IS NULL THEN 0 else B0.REV_AF_SHAR END AS REV_AF_SHAR_AMT
			FROM DTPVTT.VOL_PROFL  A0 
			LEFT JOIN DTPVTT.VOL_REV B0
			on A0.SUBR_ID = B0.SUBR_ID
		) A1
	)A2
	cross join (
		select count(*) cnt_all
		from DTPVTT.VOL_PROFL
	) B
	
)SEGMENTED BY HASH(subr_id) ALL NODES;


-------------
--IDD -------
-------------
DROP TABLE IF EXISTS DTPVTT.VOL_IDD;
CREATE TABLE DTPVTT.VOL_IDD as (
		SELECT SUBR_ID
			, SUM(SUM_OF_ACTL_DUR) AS DUR
			, SUM(NO_OF_EVENTS) AS EVENTS
		FROM DTPVTA.FCT_IDD_USG_MNTH
		WHERE DT between date '2021-10-01' and date '2021-10-31'
		AND SRC = 'CDR'
		AND SUM_OF_MOU > 0
		GROUP BY 1
)SEGMENTED BY HASH(subr_id) ALL NODES;

-----------------
--- usage -------
-----------------
---voice/data pay go 
drop table if exists DTPVTT.VOL_PAYGO;
CREATE TABLE DTPVTT.VOL_PAYGO as(
	SELECT 
	A.SUBR_ID
	, SUM(CASE WHEN CALL_TYPE_GRP = 'Voice' THEN SUM_OF_MOU ELSE 0 END) AS VC_DOM_MOU
	, SUM(CASE WHEN CALL_TYPE_GRP = 'DATA' THEN CAST(SUM_OF_ACTL_VOL AS DECIMAL(18,2))/1048576 ELSE 0 END) AS DATA_MB
	FROM DTPVTA.FCT_USG_MNTH A
	INNER JOIN DTPVTA.DIM_CALL_TYPE B
	ON A.USG_SRVC_PROD_ID = B.USG_SRVC_PROD_ID
		AND CALL_TYPE_GRP IN ('Voice', 'DATA')
		AND CALL_NATION IN ('Domestic')
	WHERE UPPER(INCMNG_OUTGNG_IND) = 'OUTGOING'
		AND A.DT between date '2021-10-01' and date '2021-10-31'
		AND UPPER(USG_TYPE) NOT IN ('BONUS USAGE')AND SRC = 'CDR'
	GROUP BY 1
)SEGMENTED BY HASH(subr_id) ALL NODES;


-------------
-- final ----
------------- 

DELETE FROM DTPVTA.MNTH_PRP_CORE_SEGMENT WHERE MNTH_ID = ${BD} ;

INSERT INTO DTPVTA.MNTH_PRP_CORE_SEGMENT  (
	select
	202110 AS MNTH_ID
   ,SUBR_ID
   ,case when IDD_user = 'IDD_user' and cust_value = 'High_value' then 'A.IDD_user_High_value'
		when IDD_user = 'IDD_user' and cust_value = 'Mid_value' then 'B.IDD_user_Mid_value'
		when IDD_user = 'IDD_user' and cust_value = 'Low_value' then 'C.IDD_user_Low_value'

		when IDD_user = 'Other_user' and data_voice_grp = 'Voice_only' and cust_value = 'High_value' then 'D.normal_vconly_High_value'
		when IDD_user = 'Other_user' and data_voice_grp = 'Voice_only' and cust_value = 'Mid_value' then 'E.normal_vconly_Mid_value'
		when IDD_user = 'Other_user' and data_voice_grp = 'Voice_only' and cust_value = 'Low_value' then 'F.normal_vconly_Low_value'

		when IDD_user = 'Other_user' and data_voice_grp <> 'Voice_only' and nontop_segm = 'topping' and cust_value = 'High_value'  then 'G.normal_VCDT_topping_High_value'
		when IDD_user = 'Other_user' and data_voice_grp <> 'Voice_only' and nontop_segm = 'topping' and cust_value = 'Mid_value' then 'H.normal_VCDT_topping_Mid_value'
		when IDD_user = 'Other_user' and data_voice_grp <> 'Voice_only' and nontop_segm = 'topping' and cust_value = 'Low_value' then 'I.normal_VCDT_topping_Low_value'

		when IDD_user = 'Other_user' and data_voice_grp <> 'Voice_only' and nontop_segm <> 'topping' and cust_value = 'High_value' then 'J.normal_VCDT_nontopping_High_value'
		when IDD_user = 'Other_user' and data_voice_grp <> 'Voice_only' and nontop_segm <> 'topping' and cust_value = 'Mid_value' then 'K.normal_VCDT_nontopping_Mid_value'
		when IDD_user = 'Other_user' and data_voice_grp <> 'Voice_only' and nontop_segm <> 'topping' and cust_value = 'Low_value' then 'L.normal_VCDT_nontopping_Low_value'
		end as core_segment
	from (
		select 
		A.subr_id
		,case when COALESCE(B.TOPPING_CNT,0) = 0 THEN 'non-topping' ELSE 'topping' END AS nontop_segm
		,case when C.pct <= 0.2 then 'High_value'
			when C.pct > 0.2  and C.pct <= 0.5 then 'Mid_value'
			when C.pct is null then 'Low_value'
			else 'Low_value' end as cust_value
		,case when E.VC_DOM_MOU > 0 and  E.DATA_MB <= 0 then 'Voice_only' else  'Other' end as data_voice_grp
		,case when D.SUBR_ID is not null then 'IDD_user' else 'Other_user' end as IDD_user
		,C.REV_AF_SHAR_AMT

		from DTPVTT.VOL_PROFL A 
		left join DTPVTT.VOL_TOPPING B
		on A.subr_id = B.subr_id
		left join DTPVTT.VOL_REV_PCT C 
		on A.subr_id = C.subr_id
		left join DTPVTT.VOL_IDD D 
		on A.subr_id = D.subr_id
		left join DTPVTT.VOL_PAYGO E
		on A.subr_id = E.subr_id
	) X1
	
)




