
CREATE TABLE ${DB_AGG}.SNP_DAIL_REWARD_COIN_EXPIRE
(
    DAY_ID date,
    USERID varchar(100),
    DTACID varchar(100),
    ExpireDate timestamp(0),
    Coins varchar(100),
    CUST_NUMB int,
    SUBR_NUMB varchar(100),
    SBSCRPN_ID int,
    SUBSCRIBER_TYPE varchar(100),
    SIM_TYPE varchar(100),
    PREFER_LANG varchar(100),
    REC_START_DT timestamp(6),
    REC_END_DT timestamp(6),
    REC_DELETED_FLAG int,
    CTL_ID varchar(10),
    INS_PROC_NAME varchar(100),
    INS_TXF_BATCHID int,
    UPD_PROC_NAME varchar(100),
    UPD_TXF_BATCHID int,
    BUSINESS_DATE date NOT NULL,
    EXECUTION_ID int,
    PROCESS_NAME varchar(600),
    DW_LAST_UPDATE_TIME timestamp(0)
) PARTITION BY (SNP_DAIL_REWARD_COIN_EXPIRE.BUSINESS_DATE);
