--- dim_bzb_mast_dtac_burn ---
CREATE TABLE ${DB_ODS}.dim_bzb_mast_dtac_burn
(
    file_id int NOT NULL,
    file_name varchar(400) NOT NULL,
    TYPE varchar(100) NOT NULL,
    CAMPAIGNID int NOT NULL,
    CAMPAIGNNAMEEN varchar(500) NOT NULL,
    STARTDATE timestamp NOT NULL,
    EXPIREDATE timestamp NOT NULL,
    SUBCATEGORYNAME varchar(100) NOT NULL,
    PARTNERNAME varchar(100) NOT NULL,
    COINS varchar(100) NOT NULL,
    SUBTYPE varchar(100) ,
    file_date date NOT NULL,
    execution_id int NOT NULL,
    load_date timestamp,
    load_user varchar(80) NOT NULL,
    process_name varchar(600) NOT NULL
)
PARTITION BY (dim_bzb_mast_dtac_burn.file_date);
