--- dwo_bzb_dtac_adjustcoin ---
CREATE TABLE ${DB_ODS}.dwo_bzb_dtac_adjustcoin
(
    FILE_ID int NOT NULL,
    FILE_NAME varchar(400) NOT NULL,
    "TIMESTAMP" timestamp NOT NULL,
    USERID varchar(100) NOT NULL,
    DTACID varchar(100) NOT NULL,
    TELTYPE varchar(100) ,
    CHANGE varchar(100) NOT NULL,
    REASON varchar(1000),
    TYPE varchar(100) NOT NULL,
    FILE_DATE date NOT NULL,
    EXECUTION_ID int NOT NULL,
    LOAD_DATE timestamp,
    LOAD_USER varchar(80) NOT NULL,
    PROCESS_NAME varchar(600) NOT NULL
)
PARTITION BY (dwo_bzb_dtac_adjustcoin.FILE_DATE);
