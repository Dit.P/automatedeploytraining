--- dim_bzb_mast_dtac_earn ---
CREATE TABLE ${DB_ODS}.dim_bzb_mast_dtac_earn
(
    file_id int NOT NULL,
    file_name varchar(400) NOT NULL,
    TYPE varchar(100) NOT NULL,
    PRODUCTID varchar(100) NOT NULL,
    CAMPAIGNNAME varchar(500) NOT NULL,
    STARTDATE timestamp NOT NULL,
    ENDDATE timestamp NOT NULL,
    SUBTYPE varchar(100) NOT NULL,
    OWNERTYPE varchar(100) NOT NULL,
    file_date date NOT NULL,
    execution_id int NOT NULL,
    load_date timestamp,
    load_user varchar(80) NOT NULL,
    process_name varchar(600) NOT NULL
)
PARTITION BY (dim_bzb_mast_dtac_earn.file_date);
