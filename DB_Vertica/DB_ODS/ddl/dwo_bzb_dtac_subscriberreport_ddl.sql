--- dwo_bzb_dtac_subscriberreport ---
CREATE TABLE ${DB_ODS}.dwo_bzb_dtac_subscriberreport
(
    FILE_ID int NOT NULL,
    FILE_NAME varchar(400) NOT NULL,
    "TIMESTAMP" timestamp NOT NULL,
    USERID varchar(100) NOT NULL,
    DTACID varchar(100) NOT NULL,
    TELTYPE varchar(100) ,
    COINS varchar(100) NOT NULL,
    CAMPAIGNID varchar(100) NOT NULL,
    SUBTYPE varchar(100) ,
    TYPE varchar(100) NOT NULL,
    FILE_DATE date NOT NULL,
    EXECUTION_ID int NOT NULL,
    LOAD_DATE timestamp,
    LOAD_USER varchar(80) NOT NULL,
    PROCESS_NAME varchar(600) NOT NULL
)
PARTITION BY (dwo_bzb_dtac_subscriberreport.FILE_DATE);
