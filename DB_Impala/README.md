# Auto Deploy

This project was big data on Database Impala.

### In the project directory, list of schema

* DB_AGG    DTDHTA  DTTHTA  DTPHTA
* DB_BASE   DTDHTB
* DB_COMMON DTDHTC
* DB_CONTROL    DTDHTL
* DB_MN DTDHTC  
* DB_ODS    DTDHTC
* DB_OPR    DTDHTA_OPR
* DB_STAGE  DTDHTT
* DB_TRG    DTDHTC

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.
* python
  ```sh
    python --version
  ```

### Installation and Build
1. Clone then repo
   ```sh
   git clone https://gitlab/repository/Project-Name.git
   ```
2. List change between Head and Head~2
    ```sh
        python util_db.py vertica vertify
    ```
3. Verify list of changes
    ```sh
        python util_db.py vertica vertify
    ```
4. Build prepare code for deploy
    ```sh
        python util_db.py vertica build
    ```
5. Deploy execute deploy to destination
    ```sh
        python util_db.py vertica deploy
    ```
6. Sanity veritfy result of deploy
    ```sh
        python util_db.py vertica sanity
    ```
<p align="right">(<a href="#top">back to top</a>)</p>

<!-- ROADMAP -->
## Roadmap

- [x] Initial to Automate Deploy
- [x] Feature/CR10001 : AR_ADJ Adjust
- [] Feature/CR10002 : SBSCRBR_APAC Error

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- CONTACT -->
## Contact

Your Name - [@your_twitter](https://twitter.com/your_username) - email@example.com

Project Link: [https://github.com/your_username/repo_name](https://github.com/your_username/repo_name)

<p align="right">(<a href="#top">back to top</a>)</p>
